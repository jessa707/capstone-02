
const jwt = require("jsonwebtoken");

// always have this secret for security measures. for unique identifier token
const secret = "EcommerceAPI";

// for creating a token of the user's details and the secret key
module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };

  return jwt.sign(data, secret, {
    // expiresIn: 60 - for session timeout
  });
};

// for verifying if the token from the user is valid based on the secret key
module.exports.verify = (request, response, next) => {
  let token = request.headers.authorization;

  // if token is recieved and not undefined
  if (typeof token !== "undefined") {
    console.log(token);

    token = token.slice(7, token.length);

    return jwt.verify(token, secret, (error, data) => {
      if (error) {
        return response.send({
          auth: "Failed.",
        });
      } else {
        // after the function, next() method will proceed to another functions
        next();
      }
    });

    //If token does not exist
  } else {
    return response.send({
      auth: "Failed.",
    });
  }
};

// For extracting the user data from the token
module.exports.decode = (token) => {
  // Token recieved and is not undefined
  if (typeof token !== "undefined") {
    // Retrieves only the token and removes the "Bearer " prefix
    token = token.slice(7, token.length);

    return jwt.verify(token, secret, (error, data) => {
      if (error) {
        return null;
      } else {
        
        return jwt.decode(token, { complete: true }).payload;
      }
    });

    // Token does not exist
  } else {
    return null;
  }
};
