// Source Connection
const express = require("express");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./router/userRoutes");
const productRoutes = require("./router/productRoutes");
const orderRoutes = require("./router/orderRoutes");

dotenv.config();

const app = express();

// MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.uk4ydtl.mongodb.net/b271-course-booking-db?retryWrites=true&w=majority`,{
  useNewUrlParser: true,
  useUnifiedTopology: true
})

let db = mongoose.connection;
db.on("open", () => console.log("Connected to MongoDB!"));
// MongoDB Connection END

// To avoid CORS error when trying to send request to our server
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

// Routes END

// app.listen(port, () => console.log(`API is now running on localhost:${port}`));
app.listen(process.env.PORT || 3000, () => {
    console.log(`Connected to localhost:${process.env.PORT || 3000}!`)
})