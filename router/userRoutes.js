const express = require("express");
const router = express.Router();
const UserController = require("../controllers/UserController");
const OrderController = require("../controllers/OrderController");
const AdminController = require("../controllers/AdminController");
const auth = require("../auth");

function adminOnleMiddleware(request, response, next) {
  const token = request.headers.authorization;
  const user = auth.decode(token);

  if (user.isAdmin) {
    next();
  } else {
    response.status(401).send("You are not an admin");
  }
}

// User register routes
router.post("/register", (request, response) => {
  UserController.register(request.body).then((result_) => {
    // console.log(result_);
    response.send({message: "User registered successfully",});
    // if you want to add to cart
    // OrderController.instantiateOrder(result_._id).then((result__) => {
    //   response.send({
    //     message: "User registered successfully",
    //   });
    // });
  });
});

// Use this to create a new admin
router.post("/registerAdmin", (request, response) => {
  AdminController.register(request.body).then((result) => {
    response.send(result);
  });
});

// User login routes
router.post("/login", (request, response) => {
  UserController.login(request.body).then((result) => {
    response.send(result);
  });
});

//Only Admin
//Get user Details 
router.get("/getAllUsersDetails", auth.verify, adminOnleMiddleware, (request, response) => {
  AdminController.getUserDetails(request.params.id).then((result) => {
      response.send(result);
  })
});

//Only Admin
// Delete User
router.delete('/delete/:id', auth.verify, adminOnleMiddleware, (request, response) => {
  AdminController.deleteTask(request.params.id).then((result) => {
    response.send(result)
  })
})

module.exports = router;
