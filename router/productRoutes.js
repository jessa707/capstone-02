const express = require("express");
const router = express.Router();
const ProductController = require("../controllers/ProductController");
const auth = require("../auth");

function adminOnleMiddleware(request, response, next) {
  const token = request.headers.authorization;
  const user = auth.decode(token);

  if (user.isAdmin) {
    next();
  } else {
    response.status(401).send("You are not an admin");
  }
}

// ONLY ADMIN
// Create
router.post("/createProduct", auth.verify, adminOnleMiddleware,(request, response) => {
    const body = request.body;

    ProductController.createProduct(
      body.name,
      body.description,
      body.price
    ).then((result) => {
      response.send(result);
    });
  }
);

// ONLY ADMIN
// Get All active or unactive product 
router.get("/getAllProducts", auth.verify, adminOnleMiddleware, (request, response) => {
    ProductController.getAllProducts().then((result) => {
      response.send(result);
    });
  }
);

// Get All active product 
router.get("/getAllActive", auth.verify, (request, response) => {
    ProductController.getAllActive().then((result) => {
      response.send(result);
    });
  }
);

// Get One 
router.get("/getOneProduct/:id", auth.verify, (request, response) => {
  const id = request.params.id;
  ProductController.getOneProduct(id).then((result) => {
    response.send(result);
  });
});

// ONLY ADMIN
// Update One (only admin)
router.patch(
  "/updateOneProduct/:id", auth.verify, adminOnleMiddleware, (request, response) => {
    const id = request.params.id;
    const body = request.body;

    ProductController.updateProduct(
      id,
      body.name,
      body.description,
      body.price
    ).then((result) => {
      response.send(result);
    });
  }
);

// ONLY ADMIN
// Archive (only admin)

router.patch("/archiveProduct/:id", auth.verify, adminOnleMiddleware,(request, response) => {
    const id = request.params.id;
    
    ProductController.archiveProduct(id).then((result) => {
      response.send(result);
    });
  }
);

router.patch("/unarchiveProduct/:id", auth.verify, adminOnleMiddleware,(request, response) => {
    const id = request.params.id;
    
    ProductController.unarchiveProduct(id).then((result) => {
      response.send(result);
    });
  }
);

//Only Admin
// Delete Product
router.delete('/deleteProduct/:id', auth.verify, adminOnleMiddleware, (request, response) => {
  ProductController.deleteTask(request.params.id).then((result) => {
    response.send(result)
  })
})

module.exports = router;
