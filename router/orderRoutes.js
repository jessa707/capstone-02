const express = require("express");
const router = express.Router();
const OrderController = require("../controllers/OrderController");
const UserController = require('../controllers/UserController')
const auth = require("../auth");

function UserOnleMiddleware(request, response, next) {
  const token = request.headers.authorization;
  const user = auth.decode(token);

  if (user.isAdmin == false) {
    next();
  } else {
    response.status(401).send("You are not an user");
  }
}





router.post('/createOrder', auth.verify, (request, response) => {
  
  const data = {
          order: request.body,
          isAdmin: auth.decode(request.headers.authorization).isAdmin
      }
  const userId = auth.decode(request.headers.authorization).id
  
  
  OrderController.createOrder(data, userId, request.body).then((result) => {
    response.send(result)
  })
})

// Retrieve all orders (Admin only)
router.get('/allOrders', (request, response) => {
  OrderController.showAllOrders().then((result) => {
    response.send(result)
  })
})




// // Get Order List
// router.get("/getOrderList", auth.verify, (request, response) => {
//   OrderController.getOrders(user.id).then((result) => {
//     response.send(result);
//   });
// });

module.exports = router;
