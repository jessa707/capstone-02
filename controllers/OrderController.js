const Order = require("../models/Order");
const User = require('../models/User')
const Product = require('../models/Product')


function adminOnleMiddleware(request, response, next) {
  const token = request.headers.authorization;
  const user = auth.decode(token);

  if (user.isAdmin) {
    next();
  } else {
    response.status(401).send("You are not an admin");
  }
}

module.exports.createOrder  = async (data, userId, newOrder) => {
        if(data.isAdmin){
           let message = Promise.resolve({
                message: 'Admin cannot place an order.'
            })
            return message.then((value) => {
                return value
            })
        }

        const new_order = new Order({
        userId: userId,
        products: newOrder.products,
        totalAmount: await totalAmount(newOrder)

        })
        return new_order.save().then((saved_newOrder) => {
            if(saved_newOrder !== null){
                return {
                    message: "Order has been successfully placed!"
                    }
                }       
            })
        }

        const totalAmount =  async (newOrder) => {
            if(newOrder.products.length > 0) {
                let subTotalAmount = 0

            for(let index = 0; index < newOrder.products.length; index++){
            
            const price = await Product.findById(newOrder.products[index].productId).then((product) => {
                if(product !=null){
                    return product.price
                    } return 0}).catch((error) => {
                    return 0
                })
                subTotalAmount += price * newOrder.products[index].quantity
                }
                return subTotalAmount
            }
        }



// Retrieve all orders (Admin only) 
module.exports.showAllOrders = () => {
    return Order.find({}).then((result, error) => {
        if(error){
        return false
        }
        return result
  })
}
